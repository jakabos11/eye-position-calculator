
#include<dlib/image_processing/frontal_face_detector.h>
#include<dlib/image_processing.h>
#include<dlib/opencv.h>
#include<opencv2/imgproc.hpp>
#include<opencv2/highgui.hpp>
#include<iostream>
#include <string>
#include <math.h>

using namespace cv;
using namespace std;
using namespace dlib;

void drawPolyline(cv::Mat& image, full_object_detection landmarks, int start, int end, bool isClosed = false) {
	std::vector<cv::Point> points;
	for (int i = start; i <= end; i++) {
		points.push_back(cv::Point(landmarks.part(i).x(), landmarks.part(i).y()));
	}
	cv::polylines(image, points, isClosed, cv::Scalar(0, 255, 255), 2, 16);

}

void drawSimpleline(cv::Mat& image, full_object_detection landmarks, int start, int end) {
	std::vector<cv::Point> points;
	points.push_back(cv::Point(landmarks.part(start).x(), landmarks.part(start).y()));
	points.push_back(cv::Point(landmarks.part(end).x(), landmarks.part(end).y()));

	cv::polylines(image, points, false, cv::Scalar(0, 0, 255), 2, 16);
	float distance = sqrtf(pow((landmarks.part(end).x() - landmarks.part(start).x()), 2) +
		pow((landmarks.part(end).y() - landmarks.part(start).y()), 2));
	string distance_str = to_string(distance);
	putText(image, distance_str, Point(50, 50), FONT_HERSHEY_SIMPLEX, 1, Scalar(0, 0, 255), 5);
}

void drawPolylines(cv::Mat& image, full_object_detection landmarks) {
	drawPolyline(image, landmarks, 36, 41, true);       //left eye
	drawPolyline(image, landmarks, 42, 47, true);       //right eye
	drawSimpleline(image, landmarks, 39, 42);
}

void findFaceLandmarksAndDrawPolylines(Mat& frame, frontal_face_detector faceDetector, shape_predictor landmarkDetector,
	std::vector<dlib::rectangle>& faces, float resizeScale, int skipFrames, int frameCounter) {
	Mat smallFrame;
	resize(frame, smallFrame, Size(), 1.0 / resizeScale, 1.0 / resizeScale);
	cv_image<bgr_pixel> dlibImageSmall(smallFrame);
	cv_image<bgr_pixel> dlibImage(frame);
	if (frameCounter % skipFrames == 0) {
		faces = faceDetector(dlibImageSmall);
	}
	for (int i = 0; i < faces.size(); i++) {
		dlib::rectangle rect(int(faces[i].left() * resizeScale),
			int(faces[i].top() * resizeScale),
			int(faces[i].right() * resizeScale),
			int(faces[i].bottom() * resizeScale));

		full_object_detection faceLandmark = landmarkDetector(dlibImage, rect);
		drawPolylines(frame, faceLandmark);
	}
}

int main() {
	VideoCapture videoCapture(0);
	if (!videoCapture.isOpened()) {
		cout << "can not open webcam" << endl;
		return 0;
	}
	frontal_face_detector faceDetector = get_frontal_face_detector();
	shape_predictor landmarkDetector;
	deserialize("C:\\Users\\jakab\\Desktop\\temalab\\temalab_test\\shape_predictor_68_face_landmarks.dat") >> landmarkDetector;
	float resizeHeight = 480;
	int skipFrames = 3;
	Mat frame;
	videoCapture >> frame;
	float height = frame.rows;
	float resizeScale = height / resizeHeight;
	double tick = getTickCount();
	int frameCounter = 0;
	namedWindow("frame", WINDOW_NORMAL);
	double fps = 30.0;
	std::vector<dlib::rectangle> faces;

	while (1) {
		if (frameCounter == 0) {
			tick = getTickCount();
		}
		videoCapture >> frame;
		findFaceLandmarksAndDrawPolylines(frame, faceDetector, landmarkDetector, faces, resizeScale, skipFrames, frameCounter);
		imshow("frame", frame);
		char key = waitKey(1);
		if (key == 27) {
			break;
		}
		frameCounter++;
		if (frameCounter == 100) {
			tick = ((double)getTickCount() - tick) / getTickFrequency();
			fps = 100.0 / tick;
			frameCounter = 0;
		}
	}

	videoCapture.release();
	destroyAllWindows();

	return 0;
}
